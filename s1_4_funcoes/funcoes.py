#!/usr/bin/env python3

import math

angulo=math.radians(30)
seno=math.sin(angulo)
cosseno=math.cos(angulo)
tangente=math.tan(angulo)

print(seno,cosseno,tangente)
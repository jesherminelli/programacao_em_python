with open('notas.csv') as notas:
    for registro in notas:
        aluno=registro.split(',')
        nome=aluno[0]
        nota1=float(aluno[1])
        nota2=float(aluno[2])
        nota3=float(aluno[3])
        media=(nota1 + nota2 + nota3) / 3
        if media>=5:
            situacao="aprovado"
        else:
            situacao="reprovado"
        print(nome,media,situacao)

#!/usr/bin/env python3

import os

# Get disk usage information
file_path = '/home/jesherpinkman/'
disk_usage = os.statvfs('/home/jesherpinkman/')
total_size = disk_usage.f_frsize * disk_usage.f_blocks
free_size = disk_usage.f_frsize * disk_usage.f_bfree
used_size = total_size - free_size

# Print disk usage information
print(f"Total disk space in {file_path}: {total_size / (1024 ** 3):.2f} GB")
print(f"Used disk space in {file_path}: {used_size / (1024 ** 3):.2f} GB")
print(f"Free disk space in {file_path}: {free_size / (1024 ** 3):.2f} GB")

#!/usr/bin/env python3

from pprint import pprint
import subprocess

def du(location: str):
    p = subprocess.Popen(["du", "-d", "1", location], stdout=subprocess.PIPE)
    out, _ = p.communicate()
    return out.decode().splitlines()


def bar_graph(du_output: list, location: str):
    du_dict = {key: float(value)
               for value, key
               in map(lambda line: line.split("\t"), du_output)}

    total_value = du_dict[location]
    del du_dict[location]

    name_width = max(len(key) for key in du_dict.keys())
    percentages = {key: value/total_value*100
                   for key, value
                   in du_dict.items()}
    print("--------------------------")
    print("percentages:")
    pprint(percentages)
    print("--------------------------")
    print("bar graph:")
    for key in sorted(percentages.keys()):
        value = round(percentages[key])
        print(f"{key:{name_width}}: {value:3d}% {'█'*round(value)}")


if __name__ == "__main__":
    location = "/home/jesher/Documentos"
    du_output = du(location)
    print("--------------------------")
    print("du_output:")
    pprint(du_output)

    bar_graph(du_output, location)

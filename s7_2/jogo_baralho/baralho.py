from random import shuffle
import carta
naipes = [carta.PAUS, carta.OUROS, carta.COPAS, carta.ESPADAS ]
class Baralho:
    def __init__(self):
        self.cartas = []
        for naipe in naipes:
            for valor in range(1,14):
                self.cartas.append(carta.Carta(naipe,valor))
    
    def __str__(self):
        texto = "" 
        for carta in self.cartas:
            texto = texto + str(carta) + "\n"
        return texto

    def embaralhar(self):
        shuffle(self.cartas) 

    def retirar(self):
        if not self.cartas:
            return None
        carta = self.cartas.pop()
        return carta


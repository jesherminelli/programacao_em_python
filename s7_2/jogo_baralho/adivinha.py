import baralho

b = baralho.Baralho()
b.embaralhar()
jogador = 0
programa = 0
empates = 0
mesmo_naipe = 0
while True:
    print("Tecle <ENTER> para escolher uma carta e \"*\" para sair")
    s = input()
    if s == "*":
        break
    c = b.retirar()
    if c is None:
        print("Fim do baralho")
        break
    print(f"Sua carta é: {c}")
    c2 = b.retirar()
    print(f"Minha carta é: {c2}")
    if c > c2:
        print("Você ganhou!")
        jogador += 1
    elif c < c2:
        print("Eu ganhei!")
        programa += 1
    else: 
        print("Empatamos!")
        empates += 1
    if c.naipe == c2.naipe:
        mesmo_naipe += 1

print(f"Vezes em que você ganhou: {jogador}")
print(f"Vezes em que você perdeu: {programa}")
print(f"Vezes em que empatamos: {empates}")
print(f"Vezes em que tiramos o mesmo naipe: {mesmo_naipe}")

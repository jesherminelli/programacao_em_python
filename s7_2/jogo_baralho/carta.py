ordem_valor = {1: "ás", 11: "valete", 12: "dama", 13: "rei"}
OUROS = "ouros"
ESPADAS = "espadas"
PAUS = "paus"
COPAS = "copas"
class Carta:
    def __init__(self, naipe, valor):
        self.naipe = naipe
        self.valor = valor
    def __eq__(self, outra):
        if isinstance(outra, Carta):
            if self.naipe == outra.naipe and \
                self.valor == outra.valor:
                return True
        return False
    
    def __str__(self):
        texto = ordem_valor[self.valor] if self.valor in ordem_valor else str(self.valor)
        return texto + " de " + self.naipe 
    
    def __gt__(self, outra):
        if isinstance(outra, Carta):
            return self.valor > outra.valor
        raise TypeError("Não é uma Carta válida") 

    def __lt__(self, outra):
        if isinstance(outra, Carta):
            return self.valor < outra.valor
        raise TypeError("Não é uma Carta válida") 